package nl.umcg.genotypetodosage;


import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;


public class GenotypeToDosageTest {

    private static Logger logger = Logger.getLogger(GenotypeToDosageTest.class);

    @Test
    public void testDetermineSampleOrder() {
        String[] input = new String[]{"S1", "S2", "S3", "S4", "S5"};
        String[] samples = new String[] {"S5", "S3", "S1"};

        int[] newOrder = GenotypeToDosage.determineSampleOrder(input, samples);

        for (int i: newOrder){
            logger.debug(i);
        }

        Assert.assertArrayEquals(new int[]{4, 2, 0}, newOrder);
    }

    @Test
    public void testReOrderSamplesString() {
        String[] input = new String[]{"S1", "S2", "S3", "S4", "S5"};
        String[] samples = new String[] {"S5", "S3", "S1"};

        int[] out = GenotypeToDosage.determineSampleOrder(input, samples);

        String[] newOrder = RegularGenotypeFileWriter.reOrderSamples(input, out);

        for (String a : newOrder) {
            logger.debug(a);
        }

        Assert.assertArrayEquals(new String[] {"S5", "S3", "S1"}, newOrder);
    }

    @Test
    public void testReOrderSamplesFloat() {
        float[] dosages = new float[]{(float)0.1, (float)0.2, (float)1, (float)1.1, (float)1.2};
        String[] input = new String[]{"S1", "S2", "S3", "S4", "S5"};
        String[] samples = new String[] {"S5", "S3", "S1"};

        int[] out = GenotypeToDosage.determineSampleOrder(input, samples);

        float[] newOrder = RegularGenotypeFileWriter.reOrderSamples(dosages, out);

        for (float a : newOrder) {
            logger.debug(a);
        }

        Assert.assertArrayEquals(new float[] {(float)1.2, (float)1.0, (float)0.1}, newOrder,(float)0.00001);
    }

    @Test
    public void testDosageToString() {
        float[] dosages = new float[]{(float)0.1, (float)0.2, (float)1, (float)1.1, (float)1.2, (float)0.5, (float) -1};

        String out = RegularGenotypeFileWriter.dosageToString(dosages," ", "NA");
        logger.debug(out);
        Assert.assertEquals(out,"0.1 0.2 1.0 1.1 1.2 0.5 NA");
    }

}
