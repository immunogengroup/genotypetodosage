package nl.umcg.genotypetodosage;

import org.molgenis.genotype.variant.GeneticVariant;

import java.io.IOException;

public class MatrixEqtlGenotypeFileWriter extends RegularGenotypeFileWriter {

    public MatrixEqtlGenotypeFileWriter(String outputPrefix, String outputSeparator, String outputNaCharacter, boolean zipOutput) throws IOException {
        super(outputPrefix, outputSeparator, outputNaCharacter, zipOutput);
    }

    @Override
    public void writeGenotype(GeneticVariant variant, String effectAllele, String alternativeAllele, float[] sampleDosages) throws IOException {
        // Write the dosages
        dosg.append(variant.getVariantId().toString())
                .append(outputSeperator)
                .append(dosageToString(sampleDosages, outputSeperator, outputNaCharacter));
        dosg.newLine();
    }

    @Override
    public void writeGenotype(GeneticVariant variant, String effectAllele, String alternativeAllele, float[] sampleDosages, int[] order) throws IOException {
        // Write the dosages
        dosg.append(variant.getVariantId().toString())
                .append(outputSeperator)
                .append(dosageToString(reOrderSamples(sampleDosages, order), outputSeperator, outputNaCharacter));
        dosg.newLine();
    }


    @Override
    public void writeVariantInfoHeader() throws IOException {
        // Write the header for the info file
        info.append("rsID")
                .append(outputSeperator)
                .append("SequenceName")
                .append(outputSeperator)
                .append("Position")
                .append(outputSeperator)
                .append("EffectAllele")
                .append(outputSeperator)
                .append("AlternativeAllele")
                .append(outputSeperator);
        info.newLine();
    }

    @Override
    public void writeVariantInfo(GeneticVariant variant, String effectAllele, String alternativeAllele, double freqZero, double freqTwo) throws IOException {
        info.append(variant.getPrimaryVariantId())
                .append(outputSeperator)
                .append(variant.getSequenceName())
                .append(outputSeperator)
                .append(Integer.toString(variant.getStartPos()))
                .append(outputSeperator)
                .append(effectAllele)
                .append(outputSeperator)
                .append(alternativeAllele);
        info.newLine();
    }
}
