package nl.umcg.genotypetodosage;

public enum GenotypeFileType {
    REGULAR,
    BIMBAM,
    MATRIX_EQTL
}
