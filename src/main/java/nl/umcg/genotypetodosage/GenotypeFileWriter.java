package nl.umcg.genotypetodosage;

import org.molgenis.genotype.variant.GeneticVariant;

import java.io.*;

public interface GenotypeFileWriter {

    void writeGentoypeHeader(String[] sampleNames) throws IOException;
    void writeGentoypeHeader(String[] sampleNames, int[] order) throws IOException;
    void writeGenotype(GeneticVariant variant, String effectAllele, String alternativeAllele, float[] sampleDosages) throws IOException;
    void writeGenotype(GeneticVariant variant, String effectAllele, String alternativeAllele, float[] sampleDosages, int[] sampleOrder) throws IOException;
    void writeVariantInfoHeader() throws IOException;
    void writeVariantInfo(GeneticVariant variant, String effectAllele, String alternativeAllele, double freqZero, double freqTwo) throws IOException;
    void writeLogHeader() throws IOException;
    void writeLog(GeneticVariant variant, String reason, boolean afMismatchRemoved) throws IOException;
    void closeStreams() throws IOException;
}
