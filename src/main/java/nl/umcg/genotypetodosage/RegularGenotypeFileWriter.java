package nl.umcg.genotypetodosage;

import org.apache.log4j.Logger;
import org.molgenis.genotype.variant.GeneticVariant;

import java.io.*;
import java.util.zip.GZIPOutputStream;

public class RegularGenotypeFileWriter implements GenotypeFileWriter {

    private static Logger LOGGER = Logger.getLogger(RegularGenotypeFileWriter.class);

    protected BufferedWriter info;
    protected BufferedWriter dosg;
    protected BufferedWriter log;
    protected String outputPrefix;
    protected String outputSeperator;
    protected String outputNaCharacter;

    public RegularGenotypeFileWriter(String outputPrefix, String outputSeparator, String outputNaCharacter, boolean zipOutput) throws IOException {
        this.outputPrefix = outputPrefix;
        this.outputSeperator = outputSeparator;
        this.outputNaCharacter = outputNaCharacter;
        this.initializeOutputStreams(zipOutput);
    }


    @Override
    public void writeGentoypeHeader(String[] sampleNames) throws IOException {
        // Write the header for the dosage file
        dosg.append("id")
                .append(outputSeperator)
                .append(String.join(outputSeperator, sampleNames));
        dosg.newLine();
    }

    @Override
    public void writeGentoypeHeader(String[] sampleNames, int[] order) throws IOException {
        // Write the header for the dosage file
        dosg.append("id")
                .append(outputSeperator)
                .append(String.join(outputSeperator, reOrderSamples(sampleNames, order)));
        dosg.newLine();
    }


    @Override
    public void writeGenotype(GeneticVariant variant, String effectAllele, String alternativeAllele, float[] sampleDosages) throws IOException {
        // Write the dosages
        dosg.append(variant.getVariantId().toString())
                .append(outputSeperator)
                .append(dosageToString(sampleDosages, outputSeperator, outputNaCharacter));
        dosg.newLine();
    }

    @Override
    public void writeGenotype(GeneticVariant variant, String effectAllele, String alternativeAllele, float[] sampleDosages, int[] sampleOrder) throws IOException {
        // Write the dosages
        dosg.append(variant.getVariantId().toString())
                .append(outputSeperator)
                .append(dosageToString(reOrderSamples(sampleDosages, sampleOrder), outputSeperator, outputNaCharacter));
        dosg.newLine();
    }

    @Override
    public void writeVariantInfoHeader() throws IOException {
        // Write the header for the info file
        info.append("rsID")
                .append(outputSeperator)
                .append("SequenceName")
                .append(outputSeperator)
                .append("Position")
                .append(outputSeperator)
                .append("EffectAllele")
                .append(outputSeperator)
                .append("AlternativeAllele")
                .append(outputSeperator)
                .append("MinorAllele")
                .append(outputSeperator)
                .append("MAF")
                .append(outputSeperator)
                .append("DosageZeroAF")
                .append(outputSeperator)
                .append("DosageTwoAF");
        info.newLine();
    }

    @Override
    public void writeVariantInfo(GeneticVariant variant, String effectAllele, String alternativeAllele, double freqZero, double freqTwo) throws IOException {
        // Write variant info
        info.append(variant.getVariantId().toString())
                .append(outputSeperator)
                .append(variant.getSequenceName())
                .append(outputSeperator)
                .append(Integer.toString(variant.getStartPos()))
                .append(outputSeperator)
                .append(effectAllele)
                .append(outputSeperator)
                .append(alternativeAllele)
                .append(outputSeperator)
                .append(variant.getMinorAllele().toString())
                .append(outputSeperator)
                .append(Double.toString(variant.getMinorAlleleFrequency()))
                .append(outputSeperator)
                .append(Double.toString(freqZero))
                .append(outputSeperator)
                .append(Double.toString(freqTwo));
        info.newLine();
    }

    @Override
    public void writeLogHeader() throws IOException {
        // Write header for log file
        log.append("VariantId")
                .append(outputSeperator)
                .append("Reason")
                .append(outputSeperator)
                .append("MAF")
                .append(outputSeperator)
                .append("MafFilterFail");
        log.newLine();
    }

    @Override
    public void writeLog(GeneticVariant variant, String reason, boolean afMismatchRemoved) throws IOException {

        log.append(variant.getPrimaryVariantId())
                .append(outputSeperator)
                .append(reason)
                .append(outputSeperator)
                .append(String.valueOf(variant.getMinorAlleleFrequency()))
                .append(outputSeperator)
                .append(String.valueOf(afMismatchRemoved));
        log.newLine();

    }

    private void initializeOutputStreams(boolean zipOutput) throws IOException {
        // Initialize the output files
        OutputStream infoFile;
        OutputStream dosgFile;
        OutputStream logFile;

        // Zip output or don't
        if (zipOutput) {
            infoFile = new GZIPOutputStream(new FileOutputStream(outputPrefix + ".variantInfo.gz"));
            dosgFile = new GZIPOutputStream(new FileOutputStream(outputPrefix + ".dosage.gz"));
            logFile = new GZIPOutputStream(new FileOutputStream(outputPrefix + ".log.gz"));
        } else {
            infoFile = new FileOutputStream(outputPrefix + ".variantInfo");
            dosgFile = new FileOutputStream(outputPrefix + ".dosage");
            logFile = new FileOutputStream(outputPrefix + ".log");
        }

        info = new BufferedWriter(new OutputStreamWriter(infoFile, "UTF-8"));
        dosg = new BufferedWriter(new OutputStreamWriter(dosgFile, "UTF-8"));
        log = new BufferedWriter(new OutputStreamWriter(logFile, "UTF-8"));
    }

    @Override
    public void closeStreams() throws IOException {

        // Close variant info file
        info.flush();
        info.close();

        // Close dosage file
        dosg.flush();
        dosg.close();

        // Close the logfile
        log.flush();
        log.close();
    }


    public static String[] reOrderSamples(String[] input, int[] order) throws IllegalArgumentException {

        String[] output = new String[order.length];

        int i = 0;
        for (int currentSample : order) {
            output[i] = input[currentSample];
            i++;
        }

        return (output);
    }

    public static float[] reOrderSamples(float[] input, int[] order) throws IllegalArgumentException {

        float[] output = new float[order.length];

        int i = 0;
        for (int currentSample : order) {
            output[i] = input[currentSample];
            i++;
        }

        return (output);
    }

    public static String dosageToString(float[] inputArray, String sep, String naString) {

        if (inputArray == null) {
            return "null";
        }

        int iMax = inputArray.length - 1;
        if (iMax == -1) {
            return "";
        }
        StringBuilder outputString = new StringBuilder();
        for (int i = 0; ; i++) {

            if (inputArray[i] < 0) {
                outputString.append(naString);
            } else {
                outputString.append(Float.toString(inputArray[i]));
            }
            if (i == iMax) {
                return outputString.toString();
            }
            outputString.append(sep);
        }
    }
}
