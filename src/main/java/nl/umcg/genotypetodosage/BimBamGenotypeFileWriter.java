package nl.umcg.genotypetodosage;

import org.apache.log4j.Logger;
import org.molgenis.genotype.variant.GeneticVariant;

import java.io.IOException;

public class BimBamGenotypeFileWriter extends RegularGenotypeFileWriter {

    private static Logger LOGGER = Logger.getLogger(BimBamGenotypeFileWriter.class);

    public BimBamGenotypeFileWriter(String outputPrefix, String outputSeparator, String outputNaCharacter, boolean zipOutput) throws IOException {
        super(outputPrefix, outputSeparator, outputNaCharacter, zipOutput);
    }

    @Override
    public void writeGentoypeHeader(String[] sampleNames) throws IOException {
        LOGGER.warn("BimBam format has no genotype header");
    }

    @Override
    public void writeGentoypeHeader(String[] sampleNames, int[] order) throws IOException {
        LOGGER.warn("BimBam format has no genotype header");
    }

    @Override
    public void writeGenotype(GeneticVariant variant, String effectAllele, String alternativeAllele, float[] sampleDosages) throws IOException {

        // Write the dosages
        dosg.append(variant.getVariantId().toString())
                .append(outputSeperator)
                .append(effectAllele)
                .append(outputSeperator)
                .append(alternativeAllele)
                .append(outputSeperator)
                .append(dosageToString(sampleDosages, outputSeperator, outputNaCharacter));
        dosg.newLine();
    }

    @Override
    public void writeGenotype(GeneticVariant variant, String effectAllele, String alternativeAllele, float[] sampleDosages, int[] order) throws IOException {

        // Write the dosages
        dosg.append(variant.getVariantId().toString())
                .append(outputSeperator)
                .append(effectAllele)
                .append(outputSeperator)
                .append(alternativeAllele)
                .append(outputSeperator)
                .append(dosageToString(reOrderSamples(sampleDosages, order), outputSeperator, outputNaCharacter));
        dosg.newLine();
    }

    @Override
    public void writeVariantInfoHeader() throws IOException {
        LOGGER.warn("BimBam format has no variant info header");
    }

    @Override
    public void writeVariantInfo(GeneticVariant variant, String effectAllele, String alternativeAllele, double freqZero, double freqTwo) throws IOException {
        // Write variant info
        info.append(variant.getVariantId().toString())
                .append(outputSeperator)
                .append(Integer.toString(variant.getStartPos()))
                .append(outputSeperator)
                .append(variant.getSequenceName());
        info.newLine();
    }
}
