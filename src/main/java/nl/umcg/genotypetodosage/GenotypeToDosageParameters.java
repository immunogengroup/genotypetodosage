package nl.umcg.genotypetodosage;

import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import java.util.List;

public final class GenotypeToDosageParameters {

    private static final Options OPTIONS;

    static {
        OPTIONS = new Options();
        Option option;

        option = Option.builder("g")
                .longOpt("genotype")
                .hasArg(true)
                .required()
                .type(String.class)
                .desc("Path to genotype data.")
                .argName("path")
                .valueSeparator(' ')
                .build();
        OPTIONS.addOption(option);

        option = Option.builder("r")
                .longOpt("reference-genotype")
                .hasArg(true)
                .type(String.class)
                .desc("Path to reference genotype data.")
                .argName("path")
                .valueSeparator(' ')
                .build();
        OPTIONS.addOption(option);

        option = Option.builder("o")
                .longOpt("output-prefix")
                .hasArg(true)
                .required()
                .type(String.class)
                .desc("Prefix for output.")
                .argName("OUTFILE")
                .valueSeparator(' ')
                .build();
        OPTIONS.addOption(option);

        option = Option.builder("k")
                .longOpt("output-type")
                .hasArg(true)
                .required()
                .desc("The output matrix format:\n"
                        + "* REGULAR - normal dosage matrix with full allele info (default)\n"
                        + "* MATRIX_EQTL - dosage matrix for matrix eQTL, (similar to regular but more succinct)\n"
                        + "* BIMBAM - BimBam formatted dosage matrix for use with GEMMA.")
                .argName("TYPE")
                .valueSeparator(' ')
                .build();
        OPTIONS.addOption(option);

        option = Option.builder("t")
                .longOpt("input-type")
                .hasArg(true)
                .required()
                .desc("The input data type. If not defined will attempt to automatically select the first matching dataset on the specified path\n"
                        + "* PED_MAP - plink PED MAP files.\n"
                        + "* PLINK_BED - plink BED BIM FAM files.\n"
                        + "* VCF - bgziped vcf with tabix index file\n"
                        + "* VCFFOLDER - matches all bgziped vcf files + tabix index in a folder\n"
                        + "* SHAPEIT2 - shapeit2 phased haplotypes .haps & .sample\n"
                        + "* GEN - Oxford .gen & .sample\n"
                        + "* TRITYPER - TriTyper format folder")
                .argName("TYPE")
                .valueSeparator(' ')
                .build();
        OPTIONS.addOption(option);

        option = Option.builder("l")
                .longOpt("reference-type")
                .hasArg(true)
                .desc("The reference data type. If not defined will attempt to automatically select the first matching dataset on the specified path\n"
                        + "* PED_MAP - plink PED MAP files.\n"
                        + "* PLINK_BED - plink BED BIM FAM files.\n"
                        + "* VCF - bgziped vcf with tabix index file\n"
                        + "* VCFFOLDER - matches all bgziped vcf files + tabix index in a folder\n"
                        + "* SHAPEIT2 - shapeit2 phased haplotypes .haps & .sample\n"
                        + "* GEN - Oxford .gen & .sample\n"
                        + "* TRITYPER - TriTyper format folder")
                .argName("TYPE")
                .valueSeparator(' ')
                .build();
        OPTIONS.addOption(option);

        option = Option.builder("s")
                .longOpt("sample-list")
                .hasArg(true)
                .type(String.class)
                .desc("List of sample names to include in output, will take the order of this file. All samples must be present in genotype data.")
                .argName("path")
                .valueSeparator(' ')
                .build();
        OPTIONS.addOption(option);

        option = Option.builder("v")
                .longOpt("variant-list")
                .hasArg(true)
                .type(String.class)
                .desc("List of variants to include in the output")
                .argName("path")
                .valueSeparator(' ')
                .build();
        OPTIONS.addOption(option);

        option = Option.builder("z")
                .longOpt("gzip")
                .type(Boolean.class)
                .desc("Gzip the output")
                .valueSeparator(' ')
                .build();
        OPTIONS.addOption(option);

    }

    public Options getOptions(){
        return OPTIONS;
    }

    public static void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.setWidth(999);
        formatter.printHelp(" ", OPTIONS);
    }
}
