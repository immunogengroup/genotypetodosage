package nl.umcg.genotypetodosage;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;
import org.molgenis.genotype.RandomAccessGenotypeData;
import org.molgenis.genotype.RandomAccessGenotypeDataReaderFormats;
import org.molgenis.genotype.variant.GeneticVariant;
import org.molgenis.genotype.variant.id.BlankGeneticVariantId;
import org.molgenis.genotype.variantFilter.VariantFilterableGenotypeDataDecorator;
import org.molgenis.genotype.variantFilter.VariantIdIncludeFilter;

import java.io.*;
import java.util.*;

/**
 * Created by olivier on 12/01/2018.
 */
public class GenotypeToDosage {

    private static Logger LOGGER = Logger.getLogger(GenotypeToDosage.class);

    public static void main(String[] args) {

        try {
            // TODO: Fix the parameter object, the current cmd implementation is ugly
            // TODO: Implement option to standardize to reference sequence
            // TODO: Implement commandline option to deal with missing variant IDs. Make chr:pos:A1>A2 the variant id
            // TODO: Write implementation to skip variants with a MAF < X when mismatched. For now it's just logged

            // Log info
            LOGGER.warn("WORK IN PROGRESS TOOL: PLEASE READ WARNINGS CAREFULLY SO NO MISUNDERSTANDING OCCURS");
            LOGGER.warn("Current implementation does NOT write variants with a missing ID field");
            LOGGER.warn("Assuming reference allele in GenotypeIO is the first allele assigned (which it should be for VCF and Trityper) but this has not been 100$ confirmed");


            // Parse commandline arguments
            CommandLineParser parser = new DefaultParser();
            CommandLine cmd = parser.parse(new GenotypeToDosageParameters().getOptions(), args);

            Set<String> variantIds = null;
            if (cmd.hasOption("v")) {
                LOGGER.warn("Using '-v': this functionality has not been fully tested, it should work but use at your own discretion");
                variantIds = readFileAsSet(cmd.getOptionValue('v').trim());
            }

            // Read the genotype data
            RandomAccessGenotypeDataReaderFormats format = RandomAccessGenotypeDataReaderFormats.valueOfSmart(cmd.getOptionValue('t').trim());
            RandomAccessGenotypeData gt = readGenotypeData(format, cmd.getOptionValue('g').trim(), variantIds);

            // Define the output type
            GenotypeFileWriter writer;
            GenotypeFileType type = GenotypeFileType.valueOf(cmd.getOptionValue('k').trim());

            // Initialize the output writer
            switch (type) {
                case BIMBAM:
                    writer = new BimBamGenotypeFileWriter(cmd.getOptionValue('o').trim(),
                            ",",
                            "NA",
                            cmd.hasOption('z'));
                    break;
                case MATRIX_EQTL:
                    writer = new MatrixEqtlGenotypeFileWriter(cmd.getOptionValue('o').trim(),
                            " ",
                            "NA",
                            cmd.hasOption('z'));
                    break;
                case REGULAR:
                    writer = new RegularGenotypeFileWriter(cmd.getOptionValue('o').trim(),
                            "\t",
                            "NA",
                            cmd.hasOption('z'));
                    break;
                default:
                    writer = new RegularGenotypeFileWriter(cmd.getOptionValue('o').trim(),
                            "\t",
                            "NA",
                            cmd.hasOption('z'));
            }

            // Read the reference data if supplied
            RandomAccessGenotypeData refGt = null;
            if (cmd.hasOption("r")) {
                RandomAccessGenotypeDataReaderFormats refFormat = RandomAccessGenotypeDataReaderFormats.valueOfSmart(cmd.getOptionValue('l').trim());
                refGt = readGenotypeData(refFormat, cmd.getOptionValue('r').trim(), variantIds);
            }

            // Determine the sample order
            int [] order = null;
            if (cmd.hasOption("s")) {
                LOGGER.warn("Using '-s': this functionality has not been fully tested, it should work but use at your own discretion");
                String[] sampleOrder = readFileAsArray(cmd.getOptionValue('s').trim());
                order = determineSampleOrder(gt.getSampleNames(), sampleOrder);
            }

            // Write the headers
            writer.writeVariantInfoHeader();
            writer.writeLogHeader();

            if (order != null) {
                writer.writeGentoypeHeader(gt.getSampleNames(), order);
            } else {
                writer.writeGentoypeHeader(gt.getSampleNames());
            }

            // Define a counter for the number of mismatching alleles
            int nAlleleMismatch = 0;
            // Count the number of missing SNPs in the reference
            int nMissSnp = 0;
            // Count the number of mismatching minor alleles
            int nMismatchMinor = 0;
            // If no reference allele was available give a warning at the end
            boolean minorAsEffect = false;

            int i = 0;
            // Loop over the genetic variants in the genotype data
            for (GeneticVariant variant : gt) {

                if ((i % 1000000) == 0) {
                    LOGGER.info("Processed " + i / 1000000 + " million records");
                }

                // If the variant ID is missing skip by default
                if (variant.getVariantId().equals(BlankGeneticVariantId.BLANK_GENETIC_VARIANT_ID)) {
                    continue;
                }

                // By default do not flip dosages
                boolean flipDosage = false;

                // Initialize the reference variant
                GeneticVariant refVar;
                if (refGt != null) {
                    refVar = refGt.getSnpVariantByPos(variant.getSequenceName(), variant.getStartPos());

                    // Check if a variant is missing in the reference data set. If so skip to the next iteration
                    // and do not output this variant in the dosage matrix
                    if (refVar == null) {
                        writer.writeLog(variant, "MissingVarInRef", false);
                        nMissSnp++;
                        continue;
                    }

                    // Confirmed that getRefAllele returns dosage 0 for TriTyper
                    String allele1 = variant.getRefAllele().toString();
                    String allele2 = variant.getAlternativeAlleles().toString();
                    String refAllele1 = refVar.getRefAllele().toString();
                    String refAllele2 = refVar.getAlternativeAlleles().toString();

                    if (allele1 == null) {
                        throw new IllegalArgumentException("Reference allele not defined in input genotype data");
                    }

                    if (refAllele1 == null) {
                        throw new IllegalArgumentException("Reference allele not defined in reference genotype data");
                    }
                    // If the variant is flipped compared to the reference data set, flip the dosages
                    // If they are not, do nothing
                    if (!allele1.equals(refAllele1) && allele1.equals(refAllele2)) {
                        flipDosage = true;
                    }

                    // If both the reference and the alt are mismatched, skip. This should not happen.
                    // If they are not mismatched but have different minor alleles, log it
                    if (!allele1.equals(refAllele1) && !allele1.equals(refAllele2)) {
                        LOGGER.warn("No matching reference or alternative. Make sure the same genome build is used " +
                                "and check if there are there non bi-allelic SNPs.");
                        continue;
                    } else if (variant.getMinorAllele() != refVar.getMinorAllele()) {

                        boolean remove = false;
                        if (variant.getMinorAlleleFrequency() < 0.4) {
                            remove = true;
                        }
                        // Write mismatching minor alleles to log
                        writer.writeLog(variant, "MinorAlleleMismatch", remove);
                        nMismatchMinor++;
                    }
                }

                // Define the reference and alternative alleles
                String effectAllele;
                // TODO: Dosage 2 equals the first allele assigned (which is usually the effec allele), but verify this
                // TODO: Refactor this to get the first allele instead of using the getRefAllele
                if (variant.getRefAllele().toString() != null) {
                     effectAllele = variant.getRefAllele().getAlleleAsString();
                } else {
                     effectAllele = variant.getMinorAllele().getAlleleAsString();
                     if (!minorAsEffect) {
                         minorAsEffect = true;
                     }
                }

                String alternativeAllele = String.join("|", variant.getAlternativeAlleles().getAllelesAsString());

                // Get dosages
                float[] sampleDosages = variant.getSampleDosages();

                // If the alleles are flipped, fix them
                if (flipDosage) {
                    nAlleleMismatch++;
                    String tmp = effectAllele;
                    effectAllele = alternativeAllele;
                    alternativeAllele = tmp;

                    // If ref and alt alleles are flipped, flip the dosages
                    for (int k = 0; k < sampleDosages.length; k++) {
                        sampleDosages[k] = 2 - sampleDosages[k];
                    }
                }

                // Calculate allele frequency's of the dosages
                // Can mismatch with genotypeIO frequencies due to different assignment of alleles vs dosages
                Multiset<Integer> set = HashMultiset.create();
                set.addAll(convertFloatsToInts(sampleDosages));

                double countZero = set.count(0);
                double countOne = set.count(1);
                double countTwo = set.count(2);

                double total = ((countZero * 2) + countOne) + ((countTwo * 2) + countOne);
                double freqZero = ((countZero * 2) + countOne) / total;
                double freqTwo = ((countTwo * 2) + countOne) / total;

                // Write the current variant to the output
                writer.writeVariantInfo(variant, effectAllele, alternativeAllele, freqZero, freqTwo);

                if (order != null) {
                    writer.writeGenotype(variant, effectAllele, alternativeAllele, sampleDosages, order);
                } else {
                    writer.writeGenotype(variant, effectAllele, alternativeAllele, sampleDosages);
                }

                i ++;
            }

            // Close the connections
            writer.closeStreams();

            if (minorAsEffect) {
                LOGGER.warn("Minor allele was used as effect allele, may not be the case." +
                        " For plink input data this is currently expected behaviour.");
            }

            // Log
            if (cmd.hasOption("r")) {
                LOGGER.info(nAlleleMismatch + " mismatching alleles detected. Flipped dosages and allele info to match");
                LOGGER.info(nMissSnp + " SNPs missing from the reference not included in the output");
                LOGGER.info(nMismatchMinor + " mismatching minor alleles, while alt and ref where the same");
            }

            LOGGER.info("Output written to files: " + cmd.getOptionValue('o').trim());

        } catch (ParseException e) {
            LOGGER.error(e);
            GenotypeToDosageParameters.printHelp();
            System.exit(1);
        } catch (IOException e) {
            LOGGER.error(e);
            System.exit(1);
        }
    }

    private static RandomAccessGenotypeData readGenotypeData(RandomAccessGenotypeDataReaderFormats format, String path, Set<String> variantIds) throws IOException {
        RandomAccessGenotypeData gt = null;

        switch (format) {
            case GEN:
                throw new UnsupportedOperationException("Not yet implemented");
            case GEN_FOLDER:
                throw new UnsupportedOperationException("Not yet implemented");
            case PED_MAP:
                throw new UnsupportedOperationException("Not yet implemented");
            case PLINK_BED:
                gt = RandomAccessGenotypeDataReaderFormats.PLINK_BED.createGenotypeData(path);
                LOGGER.warn("Using PLINK data, minor allele is assumed to be effect allele");
                LOGGER.warn("You can check the minor allele frequency's of the variantInfo file in regular mode to verify");
                break;
            case SHAPEIT2:
                throw new UnsupportedOperationException("Not yet implemented");
            case TRITYPER:
                gt = RandomAccessGenotypeDataReaderFormats.TRITYPER.createGenotypeData(path);
                break;
            case VCF:
                gt = RandomAccessGenotypeDataReaderFormats.VCF.createGenotypeData(path);
                break;
            case VCF_FOLDER:
                gt = RandomAccessGenotypeDataReaderFormats.VCF_FOLDER.createGenotypeData(path);
                break;
        }

        if (variantIds != null) {
            gt = new VariantFilterableGenotypeDataDecorator(gt, new VariantIdIncludeFilter(variantIds));
            LOGGER.info("Applied variant filter, non overlapping variants will be omitted in output");
        }

        return (gt);
    }

    public static int[] determineSampleOrder(String[] input, String[] order) {

        List<String> header = Arrays.asList(input);
        int[] outputOrder = new int[order.length];
        int i = 0;
        for (String sample : order) {
            outputOrder[i] = header.indexOf(sample);
            if (outputOrder[i] == -1) {
                LOGGER.error("Sample: \"" + sample + "\"" + " not in genotype file");
                throw new IllegalArgumentException("Sample in order not present in header");
            }
            i ++;
        }

        return (outputOrder);
    }


    public static Set<String> readFileAsSet(String file) throws IOException {
        return new HashSet<>(readFileAsList(file));
    }

    public static String[] readFileAsArray(String file) throws IOException {

        List<String> content = readFileAsList(file);
        String[] output = new String[content.size()];

        return content.toArray(output);
    }

    private static List<String> readFileAsList(String file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        List<String> content = new ArrayList<>();

        String line;
        while ((line = reader.readLine()) != null) {
            if (!line.equals("")) {
                content.add(line);
            }
        }
        reader.close();
        return(content);
    }

    private static List<Integer> convertFloatsToInts(float[] inputArray) {
        if (inputArray == null) {
            return null;
        }
        List<Integer> outputList = new ArrayList<>();
        for (int i = 0; i < inputArray.length; i++) {
            if (inputArray[i] < 0) {
                outputList.add(-1);
            } else if (inputArray[i] < 0.5) {
                outputList.add(0);
            } else {
                outputList.add(Math.round(inputArray[i]));
            }
        }
        return outputList;
    }
}
